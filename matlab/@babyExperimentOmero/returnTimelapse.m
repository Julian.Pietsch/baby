function cTimelapse=returnTimelapse(cExperiment,timelapseNum)
%cTimelapse=returnTimelapse(cExperiment,timelapseNum)
%
% loads a cTimelapse. timelapseNum should be a single number
% indicating which position in cExperiment.dirs to load.
%
% See also, EXPERIMENTTRACKING.LOADCURRENTTIMELAPSE

cExperiment.dataset.ensure_session;
posName = cExperiment.dirs{timelapseNum};
fileName = [posName 'cTimelapse_' cExperiment.rootFolder '.mat'];

% Download/update the specified cTimelapse if necessary:
localFile = cExperiment.server.downloadFile(cExperiment.dataset.dataset,fileName);

% Load the cTimelapse
l1 = load(localFile);
cTimelapse = l1.cTimelapse;

assert(isequal(posName,cTimelapse.dataset.pos),...
    'saved cTimelapse has a different position name than stored in cExperiment');

%Ensure the timelapse has the channels list for the dataset
cTimelapse.channelNames=cExperiment.channelNames;
cTimelapse.metadata = cExperiment.metadata;
cTimelapse.metadata.posname = cExperiment.dirs{timelapseNum};
cTimelapse.metadata.exptid = cExperiment.id;

% In either case, once the timelapse is successfully loaded, trigger a
% PositionChanged event to notify experimentLogging
experimentLogging.changePos(cExperiment,timelapseNum,cTimelapse);

% populate these transient properties for when new cells need to be
% detected.
cTimelapse.cCellVision = cExperiment.cCellVision;
cTimelapse.cCellMorph = cExperiment.cCellMorph;
cTimelapse.imcache = cExperiment.imcache;
end